all: clean compile run


# Generic Java 
SRC_DIR = src/main/java
BIN_DIR = bin/BookSphere

JFLAGS = -cp lib/flatlaf-3.2.5.jar -d $(BIN_DIR)

JAVA_FILES := $(shell find $(SRC_DIR) -name '*.java')

compile: $(BIN_DIR)
	javac $(JFLAGS) $(JAVA_FILES)

$(BIN_DIR):
	mkdir -p $(BIN_DIR)

run:
	java -cp $(BIN_DIR):lib/flatlaf-3.2.5.jar ch.jokulab.BookSphere.BookSphere

clean:
	rm -rf $(BIN_DIR)/*


# Maven
maven: maven-compile maven-run

maven-compile:
	mvn clean package

maven-run:
	java -jar target/BookSphere-1.0.jar


