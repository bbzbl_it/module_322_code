package ch.jokulab.BookSphere.Dialog;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MainFrame extends JFrame{

    public MainFrame() {
        // Set frame properties
        setTitle("BookSphere Main Frame");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create a panel and add components
        JPanel panel = new JPanel();
        JButton button = new JButton("Click me");
        panel.add(button);

        // Add the panel to the frame
        add(panel);
    }

    public void showMainFrame() {
        // Make the frame visible
        setVisible(true);
    }
}
