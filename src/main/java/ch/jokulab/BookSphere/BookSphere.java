package ch.jokulab.BookSphere;

import com.formdev.flatlaf.FlatDarkLaf;

import ch.jokulab.BookSphere.Dialog.MainFrame;

/**
 * BookSphere 
 */
public class BookSphere {

    public static void main(String[] args) {
        // Set FlatLaf as the look and feel
        try {
            FlatDarkLaf.setup();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Create the main frame
        MainFrame MainFrame = new MainFrame();

        // Make the frame visible
        MainFrame.show();
    }
}