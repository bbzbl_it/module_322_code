# Module_322_Code

## Getting started

__Requirements:__
- git
- java 21
- maven
- make

### Use Latest JAR from a Release
1. Go to https://gitlab.com/bbzbl_it/module_322_code/-/releases
2. Click on `Download compiled JAR file`

### Compile it yourselfe
1. Clone the repo:
```bash
git clone https://gitlab.com/bbzbl_it/module_322_code.git
```

2. Compile the code:
```bash
mvn clean package
```

3. Start the app:
```bash
make run
```

